package com.codaline.bean;

import com.codaline.helper.DbCredentials;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Getter
@Component("singleton")
public class ConnectionBean {

    private Connection connection;

    @PostConstruct
    private void buildConnection() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("db.properties");
            Properties properties = new Properties();
            properties.load(inputStream);
            DbCredentials credentials = buildCredentialsFromProperties(properties);
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(credentials.getUrl(), credentials.getUser(), credentials.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PreDestroy
    private void closeConnection() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    private DbCredentials buildCredentialsFromProperties(Properties properties) {
        DbCredentials credentials = new DbCredentials();
        credentials.setUrl(properties.getProperty("db.url"));
        credentials.setUser(properties.getProperty("db.user"));
        credentials.setPassword(properties.getProperty("db.password"));
        return credentials;
    }

}
