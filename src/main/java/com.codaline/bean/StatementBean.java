package com.codaline.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

@Component
public class StatementBean {

    @Autowired
    private ConnectionBean connectionBean;

    public Statement getStatement() {
        try {
            Connection connection = connectionBean.getConnection();
            return connection.createStatement();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
