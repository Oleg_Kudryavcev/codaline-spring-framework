package com.codaline.bean;

import com.codaline.annotations.Id;
import com.codaline.annotations.Table;
import com.codaline.enums.ColumnType;
import com.codaline.enums.Query;
import com.codaline.meta.ColumnMeta;
import com.codaline.meta.TableMeta;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Component
public class TablesCreatorBean {

    @Autowired
    private ConnectionBean connectionBean;

    public void createTables() {
        List<TableMeta> tablesMeta = new ArrayList<>();
        Reflections reflections = new Reflections("com.codaline.persistence");
        Set<Class<?>> tables = reflections.getTypesAnnotatedWith(Table.class);
        tables.forEach(table -> {
            TableMeta tableMeta = new TableMeta();
            tableMeta.setName(table.getSimpleName().toLowerCase());
            getFieldsNames(table, tableMeta);
            tablesMeta.add(tableMeta);
        });
        createTablesInDb(tablesMeta);
    }

    private void getFieldsNames(Class<?> table, TableMeta tableMeta) {
        List<Field> fields = new ArrayList<>();
        ReflectionUtils.doWithFields(table, fields::add);
        List<ColumnMeta> columnsMeta = new ArrayList<>();
        fields.stream()
                .forEach(f -> {
                    f.setAccessible(true);
                    String fieldName = f.getName();
                    String typeName = f.getType().getSimpleName();
                    String columnType = ColumnType.getDbTypeFromValue(typeName);
                    ColumnMeta columnMeta = new ColumnMeta(f.isAnnotationPresent(Id.class), fieldName, columnType);
                    columnsMeta.add(columnMeta);
                    f.setAccessible(false);
                });
        tableMeta.setColumnsMeta(columnsMeta);
    }

    private void createTablesInDb(List<TableMeta> tablesProperties) {
        try (Statement statement = connectionBean.getConnection().createStatement()) {
            List<String> createTableQueries = generateTableQuery(tablesProperties);
            createTableQueries.forEach(query -> addToBatch(statement, query));
            statement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addToBatch(Statement s, String query) {
        try {
            s.addBatch(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private List<String> generateTableQuery(List<TableMeta> tablesProperties) {
        List<String> queries = new ArrayList<>();
        tablesProperties.forEach(table -> {
            List<ColumnMeta> columns = table.getColumnsMeta();
            StringBuilder columnsBuilder = new StringBuilder();
            columns.forEach(column -> {
                columnsBuilder
                        .append(column.getName())
                        .append(" ")
                        .append(column.getType());
                if (column.isId()) {
                    columnsBuilder
                            .append(" ")
                            .append("primary key auto_increment");
                }
                columnsBuilder.append(",");
            });
            columnsBuilder.setLength(columnsBuilder.length() - 1);
            String createTableQuery = String.format(Query.CREATE_TABLE.getQuery(), table.getName(), columnsBuilder.toString());
            queries.add(createTableQuery);
        });
        return queries;
    }

}
