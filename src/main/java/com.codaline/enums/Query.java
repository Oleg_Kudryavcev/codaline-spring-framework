package com.codaline.enums;

import lombok.Getter;

public enum Query {
    INSERT("insert into %s(%s) values (%s)"),
    CREATE_TABLE("create table if not exists %s(%s)"),
    SELECT_ALL("select * from %s");

    @Getter
    private String query;

    Query(String query) {
        this.query = query;
    }
}
