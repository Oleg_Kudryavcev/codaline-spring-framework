package com.codaline.enums;

import lombok.Getter;

public enum ColumnType {
    VARCHAR("String"),
    INT("Integer");

    @Getter
    private String value;

    ColumnType(String type) {
        this.value = type;
    }

    public static String getDbTypeFromValue(String value) {
        if (VARCHAR.getValue().equals(value)) {
            return "varchar(255)";
        } else if (INT.getValue().equals(value)) {
            return "int(11)";
        } else {
            throw new IllegalArgumentException("Unrecognizable field type");
        }
    }

}
