package com.codaline;

import com.codaline.bean.TablesCreatorBean;
import com.codaline.buider.Persister;
import com.codaline.persistence.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class Application {

    private static TablesCreatorBean tablesCreatorBean;
    private static Persister persister;

    public static void main(String[] args) throws IllegalAccessException {
        SpringApplication.run(Application.class, args);
        tablesCreatorBean.createTables();

        Cat cat1 = new Cat();
        cat1.setName("Barsik");
        cat1.setAge(1);

        Cat cat2 = new Cat();
        cat2.setName("Murzik");
        cat2.setAge(2);

        persister.save(cat1);
        persister.save(cat2);

        List<Cat> cats = persister.getAll(Cat.class);
        cats.forEach(Cat::toString);

        System.exit(0);
    }

    @Autowired
    public void setTablesCreatorBean(TablesCreatorBean bean) {
        Application.tablesCreatorBean = bean;
    }

    @Autowired
    public void setPersister(Persister persister) {
        Application.persister = persister;
    }

}
