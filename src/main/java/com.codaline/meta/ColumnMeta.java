package com.codaline.meta;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ColumnMeta {

    private boolean isId;
    private String name;
    private String type;

}
