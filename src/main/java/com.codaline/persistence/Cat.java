package com.codaline.persistence;

import com.codaline.annotations.Table;
import lombok.Getter;
import lombok.Setter;

@Table
@Getter
@Setter
public class Cat extends Entity {

    private String name;
    private Integer age;

    @Override
    public String toString() {
        String catString =  "Cat{" +
                "id=" + super.getId() +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
        System.out.println(catString);
        return catString;
    }
}
