package com.codaline.persistence;

import com.codaline.annotations.Id;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Entity {

    @Id
    private Integer id;

}
