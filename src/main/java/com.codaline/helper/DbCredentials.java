package com.codaline.helper;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DbCredentials {

    private String url;
    private String user;
    private String password;

}
