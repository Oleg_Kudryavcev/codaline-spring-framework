package com.codaline.buider;

import com.codaline.bean.StatementBean;
import com.codaline.enums.Query;
import com.codaline.persistence.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Component
public class Persister {

    @Autowired
    private StatementBean statementBean;

    public <T extends Entity> List<T> getAll(Class<T> clazz) {
        String query = String.format(Query.SELECT_ALL.getQuery(), clazz.getSimpleName().toLowerCase());
        try (Statement statement = statementBean.getStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            return extractResult(clazz, resultSet);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        List<T> classInstances = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();
        int listElement = 0;
        while (resultset.next()) {
            classInstances.add(clazz.newInstance());
            classInstances.get(listElement).setId(resultset.getInt("id"));
            for (Field field : fields) {
                field.setAccessible(true);
                field.set(classInstances.get(listElement), resultset.getObject(field.getName()));
                field.setAccessible(false);
            }
            listElement++;
        }
        return classInstances;
    }

    public <T extends Entity> void save(T entity) throws IllegalAccessException {
        Field[] fields = entity.getClass().getDeclaredFields();
        StringBuilder valuesBuilder = new StringBuilder();
        StringBuilder namesBuilder = new StringBuilder();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.getName().equals("id")) continue;
            namesBuilder.append(field.getName()).append(",");
            if (field.get(entity) instanceof String) {
                valuesBuilder.append("'").append(field.get(entity)).append("'").append(",");
            } else {
                valuesBuilder.append(field.get(entity)).append(",");
            }
            field.setAccessible(false);
        }
        namesBuilder.setLength(namesBuilder.length() - 1);
        valuesBuilder.setLength(valuesBuilder.length() - 1);
        String sql = String.format(Query.INSERT.getQuery(), entity.getClass().getSimpleName().toLowerCase(), namesBuilder.toString(), valuesBuilder.toString());
        try (Statement statement = statementBean.getStatement()) {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
